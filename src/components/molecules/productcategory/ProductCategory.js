import './ProductCategory.css';
import Car from '../../../assets/car.png';

const ProductCategory = ({ product }) => {
  return (
    <>
      <div className="container mt-5" style={{ overflowX: 'auto' }}>
        <div className="row justify-content-start g-2 row-cols-lg-6 row-cols-md-4 row-cols-sm-2 row-cols-1 my-5">
          {product &&
            product.map((data, index) => {
              return (
                <div key={index} className="col">
                  <div className="card p-2">
                    <img
                      src={Car}
                      alt=""
                      style={{
                        height: '97.1719px',
                        objectFit: 'cover',
                      }}
                    />
                    <p className="product-title mb-0">{data.name}</p>
                    <p className="desc mb-0">SKU : {data.sku}</p>
                    <div className='row'>
                      <div className="col-9">
                        <p className="price">
                          {Intl.NumberFormat('id-ID', {
                            style: 'currency',
                            currency: 'IDR',
                          }).format(data.price)}
                        </p>
                      </div>
                      <div className="col-3">
                        <i className="fa-regular fa-bookmark"></i>
                      </div>
                    </div>
                  </div>
                </div>
              );
            })}
          {!product && <div>No found data</div>}
        </div>
      </div>
    </>
  );
};

export default ProductCategory;
