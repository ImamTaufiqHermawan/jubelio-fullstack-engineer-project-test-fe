import React, { useEffect, useState } from 'react';
import './Home.css';
import MyNavbar from '../../molecules/navbar/Navbar';
import ProductCategory from '../../molecules/productcategory/ProductCategory';

const Home = () => {
  const [data, setData] = useState(null);
  useEffect(() => {
    const url = 'http://localhost:3000/api/products';

    const fetchData = async () => {
      try {
        const response = await fetch(url);
        const result = await response.json();
        setData(result);
      } catch (error) {
        console.log('error adalah', error);
      }
    };

    fetchData();
  }, []);

  return (
    <>
      <MyNavbar/>
      <ProductCategory
        product={data}
    />
    </>
  );
};

export default Home;
